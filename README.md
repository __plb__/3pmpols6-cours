<img src='./cours_phelma/logo.png' height='100'/> 

# Plateforme de cours du module de preorientation SICOM - Traitement du Signal et Multimedia - 3PMPOLS6
<img src='https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc.svg' height='31'/>

___

🌐 <a href="https://__plb__.gitlab.io/3pmpols6-cours">Lien vers la **plateforme de cours**</a>

___

Ce cours est une initiation à l'intelligence artificielle **orientée image**. Il est construit en **deux parties** :

- *Partie N°1 : introduction à la notion d'image en informatique*

- *Partie N°2 : détection du contenu d'une image*