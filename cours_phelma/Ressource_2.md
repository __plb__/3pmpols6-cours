# 🪐 Ecosystème Jupyter

Le **projet Jupyter** est un projet **open source** qui vise à développer des **outils interactifs** pour la **programmation** et l'**analyse de données**. Il tire son nom des trois langages de programmation principaux pris en charge par le projet : **Ju**lia, **Py**thon et **R**.
Il comprend différents projets :
___
<a href="https://jupyter-notebook.readthedocs.io/en/latest/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="100px"/></a>

**Jupyter Notebook**

Il s'agit d'une **application web open-source** qui permet de créer et de partager des documents contenant du code exécutable, des équations, des visualisations et du texte explicatif. Utilisé pour la science des données, il combine le **code**, les **données** et leur **description** dans un seul document interactif.

___
<a href="https://jupyterlab.readthedocs.io/en/stable/" target="_blank"><img src="https://opengraph.githubassets.com/919696745a69d86f044f1320b5bb6b3864491022c63b829a28bdb3d870956653/jupyterlab/jupyterlab" alt="Jupyter Lab" height="100px"/></a>

**Jupyter Lab**

C'est une **interface utilisateur** de nouvelle génération pour le projet Jupyter. Elle offre toutes les fonctionnalités familières du Jupyter Notebook comme les notebooks, les terminaux et les éditeurs de texte mais avec une interface **plus modulaire** et **flexible**.

___
<a href="https://jupyter.org/hub" target="_blank"><img src="https://jupyterhub.readthedocs.io/en/1.0.0/_static/logo.png" alt="Jupyter Hub" height="100px"/></a>


**Jupyter Hub** (une des deux solutions retenues pour les TD de ce module avec l'utilisation de MyBinder)

Ce projet permet de déployer et de gérer des environnements Jupyter **multi-utilisateurs** sur des **serveurs** ou dans le **cloud**. Il facilite l'accès à Jupyter pour des groupes d'utilisateurs souvent utilisé dans un contexte **éducatif** ou **professionnel**.

___
<a href="https://jupyterlite.readthedocs.io/en/stable/" target="_blank"><img src="https://jupyterlite.readthedocs.io/en/stable/_static/wordmark.svg" alt="Jupyter Lite" height="100px"/></a>

**Jupyter Lite** (une des deux solutions retenues pour les TD de ce module)

JupyterLite est une distribution Jupyter **légère** qui fonctionne **entièrement dans le navigateur**. Il offre la plupart des fonctionnalités de Jupyter **sans nécessiter de serveur backend** ce qui la rend idéale pour une utilisation rapide et accessible sur des appareils avec des ressources limitées.
___
D'autres projets **gravitent** autour de Jupyter :

- <a href="https://jupyterbook.org/en/stable/intro.html" target="_blank">Jupyter Book</a> (solution retenue pour la plateforme de ce module)

Jupyter Book est un outil pour **créer des livres** et des **documents de recherche interactifs** à partir de contenu en **Markdown** et de **notebooks Jupyter** intégrant des visualisations interactives et du code exécutable.

- <a href="https://rise.readthedocs.io/en/latest/" target="_blank">Jupyter Slide</a>

Jupyter Slide, également connu sous le nom de "RISE" (Reveal.js - Jupyter/IPython Slideshow Extension), est une **extension pour Jupyter** qui permet de transformer un notebook Jupyter en une **présentation dynamique** de type diaporama, utilisant Reveal.js pour la mise en forme et la navigation.

- <a href="https://nbviewer.org/" target="_blank">nbviewer</a> (solution retenue pour afficher les TD)

Nbviewer est un **service en ligne** qui permet de **visualiser des notebooks Jupyter** stockés sur le web sous forme de **pages HTML statiques**. Il facilite le **partage** et la **visualisation de notebooks** sans nécessiter d'installation de Jupyter.

- <a href="https://nbconvert.readthedocs.io/en/latest/usage.html" target="_blank">nbconvert</a>

Nbconvert est un outil du projet Jupyter qui permet de **convertir les notebooks Jupyter** en d'autres **formats statiques** tels que HTML, PDF, Markdown. Il facilite ainsi le **partage** et la **publication** de leur contenu. Cette conversion prend en charge l'intégration des sorties de code, des graphiques et des formats de texte enrichi rendant les notebooks accessibles dans divers contextes.

- <a href="https://nbgrader.readthedocs.io/en/stable/" target="_blank">nbgrader</a>

Nbgrader est un **outil pour Jupyter** qui aide les enseignants à **créer**, **distribuer** et **noter des devoirs** sous forme de notebooks Jupyter. Il automatise la collecte, la notation et la restitution de notebooks.

- <a href="https://ipywidgets.readthedocs.io/en/stable/" target="_blank">ipywidgets</a> (solution utilisée pour le TD1)

Ipywidgets est une bibliothèque pour Jupyter qui permet de créer des **widgets interactifs** dans les notebooks. Elle facilite la création d'**interfaces utilisateur interactives** pour l'exploration de données et la visualisation. Ces widgets peuvent être utilisés pour manipuler et visualiser des données en temps réel, offrant une interaction dynamique avec le code exécuté dans le notebook.
___

Les **Jupyter notebooks** et **labs** peuvent être utilisés de **deux façons différentes** :
- *En local, sur votre ordinateur*
    - En installant l'environnement de data science Anaconda : <a href="https://docs.anaconda.com/free/anaconda/install/index.html" target="_blank">installer Jupyter via Anaconda</a> sur un ordinateur local crée un environnement complet de data science, intégrant Jupyter Notebook/Lab et d'autres outils utiles, facilitant la gestion des dépendances et des environnements isolés pour les projets de science des données.
    - En installant l'écosystème Jupyter comme bibliothèques Python : <a href="https://jupyter.org/install" target="_blank">installer Jupyter comme une bibliothèque Python via pip</a> permet d'intégrer Jupyter Notebook ou Lab dans un **environnement Python existant** offrant plus de **flexibilité** pour les **utilisateurs expérimentés** qui souhaitent personnaliser leur configuration.
    - En installant Jupyter Lab Desktop : <a href="https://github.com/jupyterlab/jupyterlab-desktop" target="_blank">Jupyter Lab Desktop</a> est une **application de bureau autonome** pour Jupyter, offrant une interface utilisateur riche **sans nécessiter un navigateur web** ce qui simplifie l'accès et l'utilisation des notebooks Jupyter sur un ordinateur local.
    - Avec Jupyter Lite

- *Sur le web, sur un serveur distant*
    - Avec Jupyter Hub déployé sur un **serveur distant privé**
    - Avec Jupyter Hub déployé sur un serveur distant d'un **fournisseur de cloud** (OVH, Google, Microsoft, AWS...)
