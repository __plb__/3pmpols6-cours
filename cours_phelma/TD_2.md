<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 💻 TD : *Classifier des images*

L'objectif de ce TD est de travailler avec des images pour **mettre en pratique** les notions vues dans le cours :
- Comprendre les réseaux de **neurones de convolution (CNN)**
- Comprendre les différences de fonctionnement entre les réseaux de neurones **denses** et de **convolution**
- Comprendre l'impact des **différentes couches de neurones** sur la **performance** des réseaux de neurones

```{note}
Différentes possibilités de travail dans l'**écosystème Jupyter** pour les TD :

- Soit en **téléchargeant le TD** et l'utilisant dans un environnement **Jupyter local** :

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/>

- Soit en utilisant **Colaboratory** (nécessite un compte Google pour exécuter les cellules mais pas pour les visualiser) :

<img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>

Pour mieux comprendre le fonctionnement de l'écosystème Jupyter, vous pouvez regarder les <a href="./Ressource_2.html">ressources complémentaires 🪐</a>
```
___

## TD 2 - Partie 1

Cette **première partie du TD 2** traite de réseaux de neurones **denses** et de leur entrainement sur des **images**.

- Exécution sur votre ordinateur (nécessite un environnement Python de data science) :

<a href="https://__plb__.gitlab.io/3pmpols6-td/-/raw/main/content/TD_2_1.ipynb?inline=false"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a> <a href="https://__plb__.gitlab.io/3pmpols6-td/-/raw/main/content/TD_2_1.ipynb?inline=false">Téléchargement du fichier du notebook du TD</a>

``````{card} 🐍 **Création de l'environnement virtuel avec conda**

Exécuter la commande suivante dans un terminal :

```bash
wget https://gitlab.com/__plb__/3pmpols6-td/-/raw/main/content/setup_td.sh && bash ~/Downloads/setup_td.sh
```

Ce script permet de récupérer tous les **fichiers des notebooks**. Il fait l'installation de l'**environnement** de science des données `Conda` avec les **bibliothèques Python** nécessaires aux TD et lance le Jupyter Lab en fin d'installation.

___

⚠️ Si vous avez **fermé la fenêtre du Jupyter Lab**, vous pouvez la **réouvrir** en exécutant la commande suivante dans un **terminal à l'emplacement** du dossier contenant les fichiers de notebooks des TD : 

```bash
conda run -n tensorflow-env jupyter lab
```

``````

- Exécution sur un serveur distant Google - Colaboratory (compte Google nécessaire pour exécuter les cellules) :

<a href="https://colab.research.google.com/github/Pierre-Loic/phelma-sicom-3PMPOLS6-td/blob/main/content/TD_2_1.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

### 🔎 Prévisualisation du TD 2 - Partie 1

<div class="container">
  <iframe class="responsive-iframe" src="https://nbviewer.org/urls/gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_2_1.ipynb" width="600" height="340" scrolling="yes" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 
___

## TD 2 - Partie 2

- Exécution sur votre ordinateur (nécessite un environnement Python de data science) :

<a href="https://__plb__.gitlab.io/3pmpols6-td/-/raw/main/content/TD_2_2.ipynb?inline=false"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a> <a href="https://__plb__.gitlab.io/3pmpols6-td/-/raw/main/content/TD_2_2.ipynb?inline=false">Téléchargement du fichier du notebook du TD</a>

- Exécution sur un serveur distant Google - Colaboratory (compte Google nécessaire pour exécuter les cellules) :

<a href="https://colab.research.google.com/github/Pierre-Loic/phelma-sicom-3PMPOLS6-td/blob/main/content/TD_2_2.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

### 🔎 Prévisualisation du TD 2 - Partie 2

<div class="container">
  <iframe class="responsive-iframe" src="https://nbviewer.org/urls/gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_2_2.ipynb" width="600" height="340" scrolling="yes" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 
___

## TD 2 - Partie 3

- Exécution sur votre ordinateur (nécessite un environnement Python de data science) :

<a href="https://__plb__.gitlab.io/3pmpols6-td/-/raw/main/content/TD_2_3.ipynb?inline=false"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a> <a href="https://__plb__.gitlab.io/3pmpols6-td/-/raw/main/content/TD_2_3.ipynb?inline=false">Téléchargement du fichier du notebook du TD</a>

- Exécution sur un serveur distant Google - Colaboratory (compte Google nécessaire pour exécuter les cellules) :

<a href="https://colab.research.google.com/github/Pierre-Loic/phelma-sicom-3PMPOLS6-td/blob/main/content/TD_2_3.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

### 🔎 Prévisualisation du TD 2 - Partie 3

<div class="container">
  <iframe class="responsive-iframe" src="https://nbviewer.org/urls/gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_2_3.ipynb" width="600" height="340" scrolling="yes" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 