<style>
  .convolution-table {
    border-collapse: separate;
    border-spacing: 10px; /* Espacement entre les cellules */
    margin: auto; /* Centrer le tableau sur la page */
  }
  .convolution-table td {
    border: 1px solid #ddd; /* Bordure plus fine */
    padding: 15px; /* Augmenter l'espacement interne pour plus de lisibilité */
    width: 50px; /* Taille uniforme des cellules */
    text-align: center;
    font-family: Arial, sans-serif; /* Police moderne */
  }
  
  /* Pour la gestion des iframes */
   .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}

.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}

@media screen and (max-width: 768px) {
    .responsive-iframe {
        display: none;
    }
}
</style>

# 📚 Cours : *Introduction à la notion d'image en informatique*

## Qu'est qu'une image en informatique ?

### Composants électroniques

```{sidebar} Capteur CCD
<a href="https://fr.wikipedia.org/wiki/Capteur_photographique_CCD" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Webcam_CCD_-_640x480px_Colour.jpg/1920px-Webcam_CCD_-_640x480px_Colour.jpg" alt="CCD" /></a>

*Source de l'image : wikipedia*
```

Une image numérique est essentiellement une **matrice de pixels** où chaque pixel représente la plus **petite unité de l'image** et contient des informations sur la **couleur** et la **luminosité**. Cette représentation matricielle est **intrinsèquement** liée à la façon dont les capteurs électroniques **capturent l'image** et comment les écrans la restituent.

Lors de la **capture d'une image**, les capteurs <a href="https://fr.wikipedia.org/wiki/Capteur_photographique_CCD" target="_blank">CCD</a> ou <a href="https://fr.wikipedia.org/wiki/Capteur_photographique#Capteur_CMOS" target="_blank">CMOS</a> transforment la lumière en un **signal électrique** à **chaque pixel**. 

```{sidebar} Ecran LCD
<a href="https://fr.wikipedia.org/wiki/Écran_à_cristaux_liquides" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/5/5d/Liquid_Crystal_Display_Macro_Example_zoom_2.jpg" alt="LCD" /></a>

*Source de l'image : wikipedia*
```

Dans le **processus de visualisation**, les écrans <a href="https://fr.wikipedia.org/wiki/Écran_à_cristaux_liquides" target="_blank">LCD</a> ou <a href="https://fr.wikipedia.org/wiki/Diode_électroluminescente_organique" target="_blank">OLED</a> prennent cette image numérique (matrice de valeurs) et la convertissent en **lumière visible**. Chaque pixel de l'écran émet la lumière selon les **valeurs de couleur** et de **luminosité spécifiées** dans la matrice de l'image reproduisant ainsi l'image pour l'œil humain.

### Taille d'une image

Une des caractéristiques importante d'une image est sa taille (ou sa définition d'affichage). La **taille d'une image** correspond au nombre de pixels qui composent la **largeur** et la **longeur** de l'image. Elle est notée de la façon suivante : *3 840 × 2 160 px* pour une <a href="https://fr.wikipedia.org/wiki/4K" target="_blank">image 4K</a> composée de 3840 pixels en largeur et 2160 pixels en hauteur soit un nombre total de pixel de 3840 x 2160 = 8 294 400 pixels.  

`````{admonition} Environnement
:class: tip
La **taille de l'image** a un impact direct sur la **mémoire** nécessaire pour **stocker**, **traiter** et **transférer** l'image. Or le **streaming vidéo** représente plus de **60%** du traffic mondial d'internet (donnée 2019).

<a href="https://fr.statista.com/infographie/21207/repartition-du-trafic-internet-mondial-par-usage/" target="_blank"><img src="https://cdn.statcdn.com/Infographic/images/normal/21207.jpeg" alt="Graphique streaming vidéo"/></a>

Etant donné l'**impact du numérique** sur les **émissions de gaz à effet de serre**, il est important de choisir correctement la **taille des images** ainsi que les **types de compression**.
`````

Pour l'**entrainement des modèles d'intelligence artificielle**, la taille des images a aussi une importance. Les photos issues des appareils photo ou des smartphones actuels (avec plusieurs millions de pixels) sont **reduites** pour **dimminuer le temps d'entrainement** des modèles. 

Une autre information intéressante est la **résolution de l'image**. Elle correspond au **nombre de pixels par unité de longeur**. Elle est exprimée en **ppp (points par pouce)** ou **dpi (dots per inch)**.

```{note}
La notion de résolution n'a de **sens** que pour l'**affichage** ou l'**impression** de l'image. Seule la **taille** de l'image a un sens pour un **fichier d'image**.
```

### Matrice d'intensités lumineuses

Pour les images en **niveaux de gris**, l'image est stockée sous forme d'une **matrice de nombres compris** généralement entre 0 et 255 (codé sur un octet) qui correspondent à l'**intensité lumineuse** : **0** correspond à du **noir** et **255** à du **blanc**. Il existe d'**autres manières** de stocker les intensités lumineuses : sous forme de **nombres à virgules flottantes** entre 0 et 1 (pour l'entrée des réseaux de neurones par exemple) ou d'**octets signés** (pour l'utilisation de la norme JPEG 2000).

```{note}

<a href="./_static/image_tableur.gif" target="_blank"><img src="./_static/image_tableur.gif" alt="Image tableur" /></a>

Une image étant un **tableau de nombres**, il est facilement possible d'afficher une image dans les **cases d'un tableur**. Il suffit de récupérer l'image au <a href="https://fr.wikipedia.org/wiki/Comma-separated_values" target="_blank">format CSV</a> et d'ajouter un dégradé de nuances de gris entre 0 et 255.
```

Pour les images en couleurs, l'image est stockée sous forme de **3 matrices** généralement entre **0** et **255** (codé sur un octet) qui correspondent à l'**intensité lumineuse** de chacune des 3 couleurs primaires **rouge**, **vert** et **bleu**. Chacune de ces 3 matrices s'appelle un **canal** (**channel** en anglais).

<a href="./_static/image_couleur.gif" target="_blank"><img src="./_static/image_couleur.gif" alt="Image en couleurs" /></a>

*Animation créée à l'aide de la bibliothèque <a href="https://www.manim.community" target="_blank">Manim</a>*

```{note}
Certaines images peuvent posséder **plus de 3 canaux**. On parle alors d'**images multispectrales** ou **hyperspectrales** suivant le nombre de canaux. Voici quelques exemples :
- les images satellites : elles possèdent en plus des canaux infrarouge qui permettent par exemple de calculer la couverture végétale d'un territoire
- les <a href="https://www.radioprotection.org/articles/radiopro/pdf/2013/04/radiopro120049.pdf" target="_blank">images de radioprotection</a> : grâce à une caméra gamma, elles possédent un canal supplémentaire pour ajouter l'information sur la radioactivité de ce qui est filmé {cite}`imagerie_gamma`
```

On peut réduire le poids des images en appliquant la **quantification** : il s'agit de réduire le nombre de teintes sur l'image.

```{card} 🎓 **Parcours à Phelma - filière SICOM**
Ces notions seront vues dans le cours <a href="https://phelma.grenoble-inp.fr/fr/formation/traitement-d-images-de-base-sicom-m1-s7-4pmstib5" target="_blank">Traitement d'images de base (SICOM S7) - 4PMSTIB5</a>.
```

### Histogramme de l'image

Pour analyser l'image, on peut tracer la distribution du **nombre de pixels** en fonction de l'**intensité lumineuse** : ce graphique s'appelle l'**histogramme de l'image**.

<a href="./_static/grey_histo.png" target="_blank"><img src="./_static/grey_histo.png" alt="Histogramme en niveaux de gris"/></a>

Aucune information sur la localisation spatiale des pixels n'est utilisée pour créer l'histogramme de l'image : **seules les intensités lumineuses** sont utilisées.

On peut aussi tracer les **histogrammes** pour les **3 canaux** d'une image en couleurs :
<a href="./_static/color_histo.png" target="_blank"><img src="./_static/color_histo.png" alt="Histogramme en couleurs"/></a>

L'**histogramme** peut être utile pour détecter des images **sur** (pic à droite de l'histogramme) ou **sous-exposées** (pic à gauche de l'histogramme).

```{admonition} Exemple d'application
L'analyse de la **distance entre les histogrammes** de différentes images d'une vidéo permet de détecter assez facilement les **changements de plan** dans la vidéo. 
```

### Sensibilité visuelle de l'oeil humain aux contrastes

La **perception** que l'être humain a des couleurs d'une image dépend non seulement de la couleur elle-même mais aussi des **autres couleurs qui l'entourent**. Cette notion est bien illustrée par la <a href="https://fr.wikipedia.org/wiki/Loi_du_contraste_simultané_des_couleurs" target="_blank">loi du contraste simultané</a>.

Lorsque deux couleurs différentes sont placées **côte à côte**, elles **s'influencent mutuellement**. Cette influence modifie la manière dont nous percevons chacune des couleurs. Par exemple, une couleur peut sembler plus claire, plus sombre, plus saturée ou d'une teinte différente lorsqu'elle est juxtaposée à une autre couleur.

Voici un **exemple d'illusion d'optique** basée sur la loi du contraste simultané :

<a href="./_static/contraste_simultane.gif" target="_blank"><img src="./_static/contraste_simultane.gif" alt="Contraste simultané"/></a>

Quand le fond est sombre, le petit carré gris à gauche **semble plus clair** qu'à droite. Il a en fait la même teinte de gris `#999999`.

`````{card} 🐍 **Bibliothèques Python**
L'image de l'illusion d'optique a été générée avec les bibliothèques <a href="https://numpy.org" target="_blank">Numpy</a> et <a href="https://matplotlib.org" target="_blank">Matplotlib</a>. Si vous souhaitez la modifier pour changer la couleur, voici le code :  
```python
import matplotlib.pyplot as plt
import numpy as np

# Tailles et couleurs des carrés
taille_grand_carre = 200
taille_petit_carre = 50
couleur_grand_carre_gauche = '#5f5f5f'
couleur_grand_carre_droit = '#ffffff'
couleur_petit_carre = '#999999'

# Créer une figure et un axe
fig, ax = plt.subplots()
ax.set_aspect('equal', adjustable='box')
ax.set_xlim([0, 2 * taille_grand_carre])
ax.set_ylim([0, taille_grand_carre])
ax.axis('off')

# Fonction pour ajouter un carré
def ajouter_carre(ax, x, y, taille, couleur):
    ax.add_patch(plt.Rectangle((x, y), taille, taille, color=couleur))

# Dessiner les grands carrés
ajouter_carre(ax, 0, 0, taille_grand_carre, couleur_grand_carre_gauche)
ajouter_carre(ax, taille_grand_carre, 0, taille_grand_carre, couleur_grand_carre_droit)

# Calculer la position des petits carrés et les dessiner
position_petit_carre = (taille_grand_carre - taille_petit_carre) / 2
ajouter_carre(ax, position_petit_carre, position_petit_carre, taille_petit_carre, couleur_petit_carre)
ajouter_carre(ax, taille_grand_carre + position_petit_carre, position_petit_carre, taille_petit_carre, couleur_petit_carre)

# Afficher l'illusion d'optique
plt.show()
```
`````

Voici un **exemple d'illusion d'optique** basée sur la loi du **contraste simultané sur de la couleur** :

<a href="./_static/contraste_simultane_couleur.gif" target="_blank"><img src="./_static/contraste_simultane_couleur.gif" alt="Contraste simultané couleur" /></a>

Même si le petit carré marron du haut **semble plus foncé**, il appartient au même rectangle vertical que le petit carré marron du bas, il a en fait la même teinte de marron  `#da7639`.

`````{card} 🐍 **Bibliothèques Python**
L'image de l'illusion d'optique a été générée avec les bibliothèques <a href="https://numpy.org" target="_blank">Numpy</a> et <a href="https://matplotlib.org" target="_blank">Matplotlib</a>. Si vous souhaitez la modifier pour changer la couleur, voici le code :  
```python
import matplotlib.pyplot as plt
import numpy as np

fig, ax = plt.subplots()

# Définir les dimensions et les couleurs des rectangles
rectangles = {
    "rect1": {"xy": (0, 0), "width": 2, "height": 1, "color": "#2090a8"},
    "rect4": {"xy": (0, 3), "width": 2, "height": 1, "color": "#ff8d48"},
    "vertical_rect": {"xy": (0.9, 0.5), "width": 0.2, "height": 3, "color": "#da7639"},
    "rect2": {"xy": (0, 1), "width": 2, "height": 1, "color": "#30086e"},
    "rect3": {"xy": (0, 2), "width": 2, "height": 1, "color": "#ffd30d"},
}

# Ajout des rectangles à la figure
for rect in rectangles.values():
    ax.add_patch(plt.Rectangle(**rect))

# Réglage des limites de l'axe pour afficher tous les rectangles
ax.set_xlim(0, 2)
ax.set_ylim(0, 4)
ax.axis('off')

# Afficher l'illusion d'optique
plt.show()
```
`````

## Comment modifier une image ?

### Sous-exposition et surexposition d'images

La sous-exposition et la surexposition sont des concepts fondamentaux en **photographie** et en **traitement de l'image**. La **sous-exposition** se produit lorsqu'une image n'a **pas reçu assez de lumière** ce qui la rend **trop sombre**. Cela peut être dû à une **<a href="https://fr.wikipedia.org/wiki/Temps_de_pose" target="_blank">durée d'exposition</a> trop court**, une **<a href="https://fr.wikipedia.org/wiki/Obturateur_(photographie)" target="_blank">ouverture</a> insuffisante** de l'objectif ou des **réglages non adaptés** de la <a href="https://fr.wikipedia.org/wiki/Sensibilité_ISO" target="_blank">sensibilité ISO</a>. A l'inverse, la **surexposition** survient lorsqu'une image reçoit **trop de lumière**, la rendant **trop claire**. Cela peut être dû à une **durée d'exposition trop long**, une **grande ouverture** de l'objectif ou des **réglages de sensibilité ISO trop élevés**.  

Par exemple, l'image suivante montre une image **sous-exposée** avec son histogramme :

<a href="./_static/sous_exposition.png" target="_blank"><img src="./_static/sous_exposition.png" alt="Sous-exposition"/></a>

Même exemple, en **surexposant** l'image :

<a href="./_static/surexposition.png" target="_blank"><img src="./_static/surexposition.png" alt="Surexposition"/></a>

### Egalisation d'histogramme

L'**égalisation d'histogramme** est un processus de traitement d'image qui vise à **améliorer la distribution des niveaux de gris** dans une image {cite}`analyse_image`. Elle permet d'**augmenter le contraste** global de l'image en égalisant la répartition des niveaux de gris. Ceci peut rendre les **détails plus visibles**. L'idée est de transformer l'histogramme de l'image en une **distribution uniforme** des niveaux de gris.

Par exemple, l'image suivante montre l'impact de l'**égalisation d'histogramme** sur une image en niveaux de gris :

<a href="./_static/egalisation.png" target="_blank"><img src="./_static/egalisation.png" alt="Egalisation"/></a>

On peut voir l'impact de l'égalisation sur la **fonction de distribution cumulative** : 

<a href="./_static/egalisation_cumul.png" target="_blank"><img src="./_static/egalisation_cumul.png" alt="Egalisation densité"></a>

### Seuillage

Le **seuillage** d'une image consiste à convertir une image en une **image binaire** en assignant une **valeur fixe** (souvent 0 ou 255) à chaque pixel en fonction de son intensité par rapport à un **seuil prédéfini** {cite}`analyse_image`. Les pixels dont l'intensité est **supérieure** au seuil sont définis à la **valeur maximale**, tandis que ceux dont l'intensité est **inférieure** au seuil sont définis à la **valeur minimale**. Cela crée une **image binaire** qui met en évidence les **régions d'intérêt** en fonction de leur **luminosité par rapport au seuil**.

$$I(x, y) = \begin{cases} 
255 & \text{si } I(x, y) > \text{Seuil} \\
0 & \text{sinon} 
\end{cases}$$

où :
- $I(x, y)$ est la **valeur de l'intensité lumineuse du pixel** de l'**image** à la position $(x, y)$

Par exemple, l'image suivante montre l'impact d'un **seuil à 200** sur une image en niveaux de gris :

<a href="./_static/seuillage.png" target="_blank"><img src="./_static/seuillage.png" alt="Seuillage" /></a>

Tous les pixels d'intensité lumineuse **supérieure à 200** sont passés à **255** et ceux dont l'intensité lumineuse est **inférieure à 200** sont passés à **0**.

### Filtres de convolution

La **convolution** est une **opération mathématique** qui consiste à **combiner deux fonctions** pour en créer une **troisième** :

$g(x) = h(x) * f(x) = \displaystyle \int_{-\infty}^{+\infty} h(\theta) f(x - \theta) d\theta$

`````{card} 🎓 **Parcours à Phelma - filière SICOM**
La notion de convolution pour un signal a déjà été vue durant le cours <a href="https://phelma.grenoble-inp.fr/fr/formation/traitement-du-signal-pet-s5-3pmesig6" target="_blank">Traitement du signal - 3PMESIG6</a>.
`````

Dans le cas de l'image, la **convolution** s'applique sur les valeurs des pixels. C'est donc la **formule discrête** de la convolution que l'on utilise :

$g(k) = h(k) * f(k) = \displaystyle \sum_{l=-\infty}^{+\infty} h(l)f(k-l)$

En l'appliquant à une image, on obtient la formule suivante pour un **noyau de convolution 3x3** :

$I_{\text{conv}}(i, j) = \displaystyle \sum_{k=-1}^{1} \sum_{l=-1}^{1} I(k, l) \cdot K(i-k, j-l)$

Avec :

- $I_{\text{conv}}(i, j)$ est la **valeur du pixel** de l'**image résultante** à la position $(i, j)$.
- $I(k, l)$ est la **valeur du pixel** de l'**image d'entrée** à la position $(k, l)$.
- $K(i-k, j-l)$ est la **valeur du pixel** du **noyau de convolution** de taille **3x3** à la position $(i-k, j-l)$.

`````{admonition} Différence entre convolution et corrélation croisée
Dans la **corrélation croisée**, le noyau **n'est pas retourné horizontalement et verticalement** comme dans la convolution. En pratique, c'est une opération de **corrélation croisée** qui est effectuée dans les bibliothèques de réseaux de neurones <a href="https://www.tensorflow.org/api_docs/python/tfc/layers/SignalConv2D" target="_blank">TensorFlow</a>, <a href="https://keras.io/api/layers/convolution_layers/convolution2d/" target="_blank">Keras</a> ou <a href="https://pytorch.org/docs/stable/generated/torch.nn.Conv2d.html" target="_blank">Pytorch</a>. 
`````

En analysant la formule précédente, on observe que les **pixels du contour** posent un problème pour calculer la convolution. Il existe **différentes solutions** pour résoudre cet **effet de bord**. On peut :

- **Ajouter des pixels** d'intensité lumineuse 0 (ou autre valeur calculée par rapport aux pixels de l'image) en périphérie de l'image pour pouvoir effecttuer tous les calculs (mode "same" si la taille de l'image de sortie est identique à l'entrée et mode "full" si l'image de sortie est plus grande)

- **Ne pas calculer** les convolutions pour les pixels du bord de l'image ce qui **réduit à taille** de l'image de sortie (mode "valid")

`````{card} 🐍 **Bibliothèques Python**
Pour illustrer les **différentes solutions** pour résoudre les **effets de bord**, on peut utiliser la bibliothèque <a href="https://docs.scipy.org/doc/scipy/" target="_blank">Scipy</a> (ces choix se retrouvent dans le paramètre "padding" des bibliothèques de réseaux de neurones comme <a href="https://keras.io/getting_started/" target="_blank">Keras</a>). Voici un exemple pour illustrer les 3 modes "full", "same" et "valid" :

```python
import numpy as np
from scipy.signal import convolve2d

# Définition des tableaux d'entrée
array1 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
array2 = np.array([[1, 0], [0, 1]])
print("Tableau N°1 :\n", array1)
print("Tableau N°2 :\n", array2)

# Convolution en mode 'full'
result_full = convolve2d(array1, array2, mode='full')
print("Mode 'full' :\n", result_full)

# Convolution en mode 'same'
result_same = convolve2d(array1, array2, mode='same')
print("\nMode 'same' :\n", result_same)

# Convolution en mode 'valid'
result_valid = convolve2d(array1, array2, mode='valid')
print("\nMode 'valid' :\n", result_valid)
```
```
Tableau N°1 :
 [[1 2 3]
 [4 5 6]
 [7 8 9]]

Tableau N°2 :
 [[1 0]
 [0 1]]

Mode 'full' :
 [[ 1  2  3  0]
 [ 4  6  8  3]
 [ 7 12 14  6]
 [ 0  7  8  9]]

Mode 'same' :
 [[ 1  2  3]
 [ 4  6  8]
 [ 7 12 14]]

Mode 'valid' :
 [[ 6  8]
 [12 14]]
```

`````

Les filtres de convolution sont très souvent de **taille impaire** (3x3, 5x5, 7x7...) pour deux raisons pricipales :
- Avoir un **pixel central** sur lequel est calculé la convolution
- Avoir un **centrage du filtre** avec autant de pixels dans toutes les directions de calcul de la convolution

Les filtres de convolutions peuvent être utilisés dans **différents cas d'application** :
- Détecter des contours
- Augmenter le contraste
- Lisser les images
- Réduire le bruit

### Différents filtres de convolution

#### Filtres de moyenne et de médiane

Il s'agit d'un **filtre passe-bas** (il réduit les variations rapides d'intensité lumineuse) qui permet de **diminuer le bruit** mais diminue aussi les **détails de l'image (flou)**. Il consiste à faire la **moyenne des intensités** des pixels dans le **voisinage du pixel** sur lequel est centré le filtre. En taille **3x3**, il a la forme suivante :

<table class="convolution-table">
  <tr>
    <td style="background-color: #0080FF">1/9</td>
    <td style="background-color: #0080FF">1/9</td>
    <td style="background-color: #0080FF">1/9</td>
  </tr>
  <tr>
    <td style="background-color: #0080FF">1/9</td>
    <td style="background-color: #0080FF">1/9</td>
    <td style="background-color: #0080FF">1/9</td>
  </tr>
  <tr>
    <td style="background-color: #0080FF">1/9</td>
    <td style="background-color: #0080FF">1/9</td>
    <td style="background-color: #0080FF">1/9</td>
  </tr>
</table>

La taille du filtre de moyenne a un impact sur le lissage de l'image : plus la **taille du filtre** est **grande**, plus le **lissage de l'image** est **important**.

Voici une **comparaison de 3 filtres** de moyenne de tailles différentes **3x3**, **7x7** et **11x11** :

<a href="./_static/mean.png" target="_blank"><img src="./_static/mean.png" alt="Filtre de moyenne"/></a>

Sur une **image bruitée**, on observe le **compromis** entre le **lissage du bruit** et la **perte des détails** de l'image en augmentant la taille des filtres de moyenne :

<a href="./_static/mean_noise.png" target="_blank"><img src="./_static/mean_noise.png" alt="Filtre de moyenne avec bruit"/></a>

Pour effectuer une **réduction de bruit** sur une image, il est légèrement plus efficace d'utiliser un **filtre de médiane** qu'un filtre de moyenne. En effet, le filtre de médiane est **moins impacté** par les **valeurs extrêmes** d'intensité lumineuse : il permet de **conserver les contours** des élèments de l'image.

Voici un exemple de comparaison de filtres **7x7** de **moyenne** et de **médiane** sur une image bruitée :

<a href="./_static/mean_median_noise.png" target="_blank"><img src="./_static/mean_median_noise.png" alt="Filtre de moyenne et de médiane"/></a>

#### Filtre gaussien

Le **filtre gaussien** est aussi de type **passe-bas** mais il permet un **lissage plus doux** de l'image qu'avec un filtre de moyenne {cite}`filtrage`. Contrairement au filtre de moyenne qui s'appuie sur des coefficients uniformes, le **filtre gaussien** utilise des coefficients calculés à partir de la formule suivante (distribution normale) :

$$G(x, y) = \frac{1}{2\pi\sigma^2} \exp\left(-\frac{x^2 + y^2}{2\sigma^2}\right)$$

Avec :

- G(x, y) : représente la valeur du **noyau gaussien** à une position donnée (x, y)
- $\sigma$ est l'écart-type de la distribution gaussienne, qui **contrôle la largeur** de la cloche de la distribution et donc l'**intensité du flou**
- $\pi$ est la **constante mathématique** pi

On peut calculer les coefficients du filtre gaussien de **taille 3x3** et d'**écart-type $\sigma = 1$**.

$G(0, 0) = \frac{1}{2\pi \cdot 1^2} \exp\left(-\frac{0^2 + 0^2}{2 \cdot 1^2}\right) = \frac{1}{2\pi} \approx 0.15917$

$G(0, 1) = G(1, 0) = G(0, -1) = G(-1, 0) = \frac{1}{2\pi \cdot 1^2} \exp\left(-\frac{1^2 + 0^2}{2 \cdot 1^2}\right) \approx 0.09653$

$G(1, 1) = G(-1, 1) = G(1, -1) = G(-1, -1) = \frac{1}{2\pi \cdot 1^2} \exp\left(-\frac{1^2 + 1^2}{2 \cdot 1^2}\right) \approx 0.05855$

On obtient le **filtre de convolution 3x3** suivant :

<table class="convolution-table">
  <tr>
    <td style="background-color: #e6f3ff">0.05855</td>
    <td style="background-color: #66b2ff">0.09653</td>
    <td style="background-color: #e6f3ff">0.05855</td>
  </tr>
  <tr>
    <td style="background-color: #66b2ff">0.09653</td>
    <td style="background-color: #0056b3">0.15917</td>
    <td style="background-color: #66b2ff">0.09653</td>
  </tr>
  <tr>
    <td style="background-color: #e6f3ff">0.05855</td>
    <td style="background-color: #66b2ff">0.09653</td>
    <td style="background-color: #e6f3ff">0.05855</td>
  </tr>
</table>

`````{card} 🐍 **Bibliothèques Python**

Les bibliothèques <a href="./Ressource_1.html#bibliotheques-specialisees-dans-l-image">OpenCV</a> et <a href="./Ressource_1.html#bibliotheques-specialisees-dans-l-image">Pillow</a> permettent d'appliquer un **filtre gaussien** sur une image. Le paramètre à fournir à la fonction Python est l'**écart-type de la gaussienne** : la **taille du filtre** est définie par rapport à cet écart-type (plus l'écart-type est grand plus le filtre est grand). Voici un exemple de code avec <a href="./Ressource_1.html#bibliotheques-specialisees-dans-l-image">Pillow</a> :  
```python
import matplotlib.pyplot as plt
from PIL import Image, ImageFilter
import urllib.request
import io

# Télécharger l'image depuis une URL
url = 'https://phelma.grenoble-inp.fr/medias/photo/20160923-122641-panoramique-_1482153339359-jpg'
with urllib.request.urlopen(url) as response:
    image_data = response.read()
image = Image.open(io.BytesIO(image_data)).convert('L')

# Appliquer un filtre gaussien
image_filtree = image.filter(ImageFilter.GaussianBlur(3))

# Afficher l'image originale et l'image filtrée
plt.figure(figsize=(8, 4))
plt.subplot(121)
plt.title('Image originale')
plt.imshow(image, cmap="gray")

plt.subplot(122)
plt.title('Image filtrée')
plt.imshow(image_filtree, cmap="gray")

plt.show()
```
<img src="./_static/gaussian.png" alt="Filtre gaussien" height="130px"/>

`````
L'écart-type du filtre gaussien a un impact sur le lissage de l'image : plus l'**écart-type** est **grand**, plus le **lissage de l'image** est **important**.

Voici une **comparaison de 3 filtres** gaussiens d'écart-types différents **1**, **3** et **5** :

<a href="./_static/gaussian_filters.png" target="_blank"><img src="./_static/gaussian_filters.png" alt="Filtre gaussiens"/></a>

Sur une **image bruitée**, on observe le **compromis** entre le **lissage du bruit** et la **perte des détails** de l'image en augmentant la valeur des écart-types des filtres gaussiens :

<a href="./_static/gaussian_filters_noise.png" target="_blank"><img src="./_static/gaussian_filters_noise.png" alt="Filtre gaussiens avec bruit"/></a>

#### Filtre gradient

Le **filtre de gradient** est un **filtre passe-haut** qui est utilisé pour **détecter les variations d'intensité** des pixels d'une image : il met en évidence les **contours** et les **transitions de couleur** dans les directions **horizontale** et **verticale**. **Deux filtres** sont couramment utilisés pour cette méthode les <a href="https://fr.wikipedia.org/wiki/Filtre_de_Sobel" target="_blank">filtres de Sobel</a> et de <a href="https://fr.wikipedia.org/wiki/Filtre_de_Prewitt" target="_blank">Prewitt</a>.

Le filtre de **Sobel vertical** correspond au noyau de convolution de **taille 3x3** suivant :

<table class="convolution-table">
  <tr>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
  <tr>
    <td style="background-color: #FF0000">-2</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #0080FF">2</td>
  </tr>
  <tr>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
</table>

Voici un exemple de l'utilisation d'un **filtre de Sobel vertical** sur une image en niveaux de gris :

<a href="./_static/sobel_v.png" target="_blank"><img src="./_static/sobel_v.png" alt="Filtre de Sobel vertical" /></a>

On observe que le **filtre de Sobel vertical** a **fait ressortir** les élèments verticaux de l'image comme les montants verticaux de **fenêtres** du bâtiment M de Minatec et le trottoir. 

Le filtre de **Sobel horizontal** correspond au noyau de convolution de **taille 3x3** suivant :

<table class="convolution-table">
  <tr>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #FF0000">-2</td>
    <td style="background-color: #FF9999">-1</td>
  </tr>
  <tr>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #8E8E8E">0</td>
  </tr>
  <tr>
    <td style="background-color: #99CCFF">1</td>
    <td style="background-color: #0080FF">2</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
</table>

Voici un exemple de l'utilisation d'un **filtre de Sobel horizontal** sur une image en niveaux de gris :

<a href="./_static/sobel_h.png" target="_blank"><img src="./_static/sobel_h.png" alt="Filtre de Sobel horizontal" /></a>

On observe que le **filtre de Sobel horizontal** a **fait ressortir** les élèments horizontaux de l'image comme les montants horizontaux de **fenêtres** du bâtiment M de Minatec et le trottoir. 

Les filtres de **Prewitt** sont **très proches** des filtres de **Sobel** : seuls **deux coefficients** sont modifiés. Cette différence rend les filtres de Sobel **meilleurs** que les filtres de Prewitt pour détecter les **contours obliques** mais **moins uniforme**.

Le filtre de **Prewitt vertical** correspond au noyau de convolution de **taille 3x3** suivant :

<table class="convolution-table">
  <tr>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
  <tr>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
  <tr>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
</table>

Le filtre de **Prewitt horizontal** correspond au noyau de convolution de **taille 3x3** suivant :

<table class="convolution-table">
  <tr>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #FF9999">-1</td>
    <td style="background-color: #FF9999">-1</td>
  </tr>
  <tr>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #8E8E8E">0</td>
  </tr>
  <tr>
    <td style="background-color: #99CCFF">1</td>
    <td style="background-color: #99CCFF">1</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
</table>

#### Filtre laplacien

Le **filtre laplacien** est un **filtre passe-haut** qui permet de détecter les **variations rapides** de luminosité sur une image. En mathématique, le laplacien correspond à la **somme des dévivées secondes** suivant les différentes dimensions (dans le cas des images, les dimensions sont la **hauteur** et la **largeur** de l'image) :

$$\Delta L(x, y) = \frac{\partial^2 I(x,y)}{\partial x^2} + \frac{\partial^2 I(x,y)}{\partial y^2}$$

Si on utilise une **approximation discrète de la dérivée seconde** par rapport à la **hauteur** et la **largeur** de l'image pour calculer le laplacien, on obtient le noyau de convolution suivant :

<table class="convolution-table">
  <tr>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #99CCFF">1</td>
    <td style="background-color: #8E8E8E">0</td>
  </tr>
  <tr>
    <td style="background-color: #99CCFF">1</td>
    <td style="background-color: #990000">-4</td>
    <td style="background-color: #99CCFF">1</td>
  </tr>
  <tr>
    <td style="background-color: #8E8E8E">0</td>
    <td style="background-color: #99CCFF">1</td>
    <td style="background-color: #8E8E8E">0</td>
  </tr>
</table>

Voici un exemple de l'utilisation d'un **filtre laplacien** sur une image en niveaux de gris :


<a href="./_static/laplacien.png" target="_blank"><img src="./_static/laplacien.png" alt="Filtre laplacien" /></a>

On observe que le **filtre laplacien** a **fait ressortir** tous les **contours** des élèments de l'image : les bâtiments A et M de Minatec, le trottoir, les personnes...

___
## Support de présentation du cours

<a href="./_static/slides/cours_Images.pdf" target="_blank">👨‍🏫 Support de cours N°1</a>

<div class="container">
  <iframe class="responsive-iframe" src="./_static/slides/cours_Images.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>